#!/bin/sh

docker build -t exam-mcs-nodejs-container .
if [ $? -eq 1 ]; then
	echo "Error building image."
	exit 1
else
    docker run -d -p 8080:8080 exam-mcs-nodejs-container
fi