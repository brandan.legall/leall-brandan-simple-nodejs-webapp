FROM node:latest

WORKDIR /usr/src/app

COPY src/count-server.js ./

EXPOSE 8080

ENV APPLICATION_INSTANCE=example

CMD ["node", "count-server.js"]
